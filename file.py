# r : 읽기모드 - 파일을 읽기만 할때 사용
# w : 쓰기모드 - 파일에 내용을 쓸 때 사용
# a : 추가모드 - 파일의 마지막에 새로운 내용을 추가 시킬 때 사용

# 1.
# f = open("newfile.txt",'w')
# f.close()

# 2. it is %d row .\n개 출력
# f = open("file.txt",'w')
# for i in range(1,11):
#     data = "it is %d row .\n" %i
#     f.write(data)
# f.close()

# 2. 한국어를 사용할 때
# f = open("koreafile.txt",'w',encoding="UTF-8")
# for i in range(1,11):
#     data = "%d개 .\n" %i
#     f.write(data)
# f.close()

# 이렇게 사용하면 f.close() not add
with open("file.txt","w") as f:
    f.write("Life is too short, you need python..?")
# --------------- w

# one line view
f = open("file.txt",'r')
line = f.readline()
print(line)
f.close

# 1.All
f = open("file.txt",'r')
while True:
    line = f.readline()
    if not line: break
    print(line)
f.close
# 2.All
f = open("file.txt",'r')
lines = f.readlines()
for line in lines:
    print(line)
f.close

# 3.All
f = open("file.txt",'r')
data = f.read()
print(data)
f.close

# -----------r

f = open("file.txt",'a')
for i in range(11,20):
    data = "it is %d row ." %i
    f.write(data)
f.close