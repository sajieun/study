def sum(a,b):
    result = a + b
    return result
print(sum(1,2))

def say():
    return 'Hi'
print(say())

# 이런식으로 꼭 return값이 없어도 된다.
def sum2(a,b):
    print("%d, %d의 합은 %d입니다." %(a,b,a+b))
print(sum2(1,2))

# 이렇게 넣으면 내가 원하는 값 마음대로 넣을 수 있음
def sum_many(*args):
    sum3 = 0
    for i in args:
        sum3 = sum3 + i
    return sum3
print(sum_many(1,2,3,4))
print(sum_many(1,2,3,5,4,3,4,))

# 뒷 내용에 더 자세히 알려준다고 함
def print_kwargs(**kwargs):
    for k in kwargs.keys():
        if(k == "name"):
            print("당신의 이름은 :" + k)
print(print_kwargs(name="int 조수", b="2"))

# 저렇게 true를 사용하고 싶은건 맨 뒤에다가 쓰는게 좋다
def say_myself(name,old,man=True):
    print("my name is %s" %name)
    print("my ags is %d" % old)
    if man:
        print("man")
    else:
        print("woman")
say_myself('sajieun',24,False)

# 지역변수의 예시를 보여준느것아다 곧 def에 쓰인건 밖에있는 a에 영향이 미치지 않는다.
a = 1
def vartest(a):
    a = a+1
vartest(a)
print(a)

# 1. 지역변수를 밖에 있는 값에 영향이 가게끔 할 수 있게 하는 방법
b = 1
def vartest2(b):
    b = b+1
    return b
b = vartest2(b)
print(b)
print('-----')

# 2. 지역변수를 밖에 있는 값에 영향이 가게끔 할 수 있게 하는 방법
c = 1
def vartest3():
    global c
    c = c + 1
vartest3()
print(c)

# 람다식 정의
# def add(a,b):
#     return a+b
add = lambda a,b : a+b
print(add(1,2))

# input 함수
d = input()
print(d)